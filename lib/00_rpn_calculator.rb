class RPNCalculator
  # TODO: your code goes here!
  attr_accessor :number


  def initialize
    @stack= []
  end

  def push(n)
    @stack <<n
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    array = []
    string.split(" ").each do |x|
      case x
      when "-"
        array << :-
      when "+"
        array << :+
      when "/"
        array << :/
      when "*"
        array << :*
      else
        array << x.to_i
      end
    end
      array
  end

  def evaluate(string)
    tokens(string).each do |x|
      case x
      when Integer
        push(x)
      else
        perform_operation(x)
      end

    end
    value
  end

  def perform_operation(op)
    raise "calculator is empty" if @stack.size <2
    second_operand = @stack.pop
    first_operand = @stack.pop

    case op
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand / second_operand.to_f
    else
      @stack << first_operand
      @stack << second_operand
      raise "No such operation #{op}"
    end
  end
end
